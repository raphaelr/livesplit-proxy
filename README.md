# livesplit-proxy - Remote control for one.livesplit.org

## Usage

1. Download the .NET 5 SDK from https://dotnet.microsoft.com/download
2. Run with `dotnet run`
3. Open https://one.livesplit.org
4. Click on "Connect to Server", and enter `ws://localhost:8081`
5. Control livesplit via `curl http://localhost:8081/?q=COMMAND`

List of commands: https://github.com/LiveSplit/LiveSplitOne/blob/6c7281e110449c07e53e3807fbde19a8beb7aa94/src/ui/TimerView.tsx#L254

Most common commands: `splitorstart`, `undo`, `skip`, `reset`

You can change the port this application is listening on with the environment variable
DOTNET_URLS, and the --no-launch-profile option. For example:

    DOTNET_URLS=http://localhost:7777 dotnet run --no-launch-profile

## License

Copyright 2021 Raphael Robatsch

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
