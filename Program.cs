// livesplit-proxy - one.livesplit.org remote control
// See COPYING file for copyright and license details.

using System.Collections.Generic;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace livesplit_proxy
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
    
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<Hub>();
            services.AddSingleton<IHostedService>(sp => sp.GetRequiredService<Hub>());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseWebSockets();
            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.Map("/", async context =>
                {
                    var hub = context.RequestServices.GetRequiredService<Hub>();
                    if (context.WebSockets.IsWebSocketRequest)
                    {
                        var ws = await context.WebSockets.AcceptWebSocketAsync();
                        var completion = new TaskCompletionSource();
                        await hub.Post(HubMessage.AddSocket(ws, completion));
                        await completion.Task;
                        return;
                    }
                    
                    var q = context.Request.Query["q"];
                    if (q.Count > 0)
                    {
                        await hub.Post(HubMessage.Send(q[0]));
                        return;
                    }

                    await context.Response.WriteAsync("Error: Either send a WebSocket request, or a 'q' query string parameter.");
                });
            });
        }
    }

    public class HubClient
    {
        private static int _nextId;
        
        public int Id { get; }
        public WebSocket WebSocket { get; }
        public TaskCompletionSource Completion { get; }

        public HubClient(WebSocket ws, TaskCompletionSource completion)
        {
            Id = Interlocked.Increment(ref _nextId);
            WebSocket = ws;
            Completion = completion;
        }
    }

    public enum HubMessageType
    {
        AddSocket,
        RemoveSocket,
        Send,
    }

    public class HubMessage
    {
        public HubMessageType Type { get; }
        public object Value { get; }

        private HubMessage(HubMessageType type, object value)
        {
            Type = type;
            Value = value;
        }

        public static HubMessage AddSocket(WebSocket ws, TaskCompletionSource completion) => new HubMessage(HubMessageType.AddSocket, new HubClient(ws, completion));
        public static HubMessage RemoveSocket(WebSocket ws) => new HubMessage(HubMessageType.RemoveSocket, ws);
        public static HubMessage Send(string line) => new HubMessage(HubMessageType.Send, line);
    }

    public class Hub : IHostedService
    {
        private readonly ILogger<Hub> _log;
        
        private readonly Channel<HubMessage> _channel;
        private readonly TaskCompletionSource _completion;
        private readonly List<HubClient> _clients;

        public Hub(ILogger<Hub> log, IHostApplicationLifetime lifetime)
        {
            _log = log;
            lifetime.ApplicationStopping.Register(() => _channel.Writer.TryComplete());

            _channel = Channel.CreateUnbounded<HubMessage>();
            _completion = new TaskCompletionSource();
            _clients = new List<HubClient>();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _ = RunAsync();
            return Task.CompletedTask;
        }

        private async Task RunAsync()
        {
            await foreach (var item in _channel.Reader.ReadAllAsync())
            {
                switch (item.Type)
                {
                    case HubMessageType.AddSocket:
                    {
                        var client = (HubClient)item.Value;
                        HandleAddSocket(client);
                        break;
                    }
                    case HubMessageType.RemoveSocket:
                    {
                        var target = (WebSocket)item.Value;
                        HandleRemoveSocket(target);
                        break;
                    }
                    case HubMessageType.Send:
                    {
                        var line = (string)item.Value;
                        await HandleSend(line);
                        break;
                    }
                }
            }
            foreach (var client in _clients)
            {
                client.Completion.TrySetResult();

            }
            _completion.TrySetResult();
        }

        private async Task HandleSend(string line)
        {
            _log.LogDebug("Sending {Line} to {N} clients", line, _clients.Count);
            var bytes = Encoding.UTF8.GetBytes(line);
            foreach (var client in _clients)
            {
                try
                {
                    await client.WebSocket.SendAsync(bytes, WebSocketMessageType.Text, true, CancellationToken.None);
                }
                catch (WebSocketException ex)
                {
                    _log.LogInformation(ex, "WS {Id} failed during send, removing", client.Id);
                }
            }
        }

        private void HandleAddSocket(HubClient client)
        {
            _log.LogInformation("WS {Id} connected", client.Id);
            _clients.Add(client);
            _ = ReceiveHandler(client);
        }

        private void HandleRemoveSocket(WebSocket target)
        {
            for (var index = 0; index < _clients.Count; index++)
            {
                var client = _clients[index];
                if (client.WebSocket == target)
                {
                    _clients.RemoveAt(index);
                    client.Completion.SetResult();
                    break;
                }
            }
        }

        private async Task ReceiveHandler(HubClient client)
        {
            var buf = new byte[16];
            try
            {
                while (client.WebSocket.State == WebSocketState.Open)
                {
                    var res = await client.WebSocket.ReceiveAsync(buf, default);
                    if (res.MessageType == WebSocketMessageType.Close)
                    {
                        _log.LogInformation("WS {Id} closed the connection, removing", client.Id);
                        await Post(HubMessage.RemoveSocket(client.WebSocket));
                    }
                }
            }
            catch (WebSocketException ex)
            {
                _log.LogInformation(ex, "WS {Id} failed during receive, removing", client.Id);
                await Post(HubMessage.RemoveSocket(client.WebSocket));
            }
        }

        public async Task Post(HubMessage message)
            => await _channel.Writer.WriteAsync(message);

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _channel.Writer.TryComplete();
            return _completion.Task;
        }
    }
}
